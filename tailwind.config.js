/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './packages/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      flexBasis: {
        '21/100': '21%',
      },
      zIndex: {
        2: 2,
      },
      colors: {
        gray: '#8492a6',
      },
      flexGrow: {
        2: '2',
        3: '3',
      },
      rotate: {
        7: '7deg',
      },
      fontFamily: {
        sans: ['Ubuntu', 'sans-serif'],
        ubuntu: ['Ubuntu', 'sans-serif'],
        satisfy: ['Satisfy', 'sans-serif'],
      },
      spacing: {
        1: '4px',
        2: '8px',
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
  },
  plugins: [],
};
