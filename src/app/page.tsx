import { TypingAnimation } from '@journey-planner/shared-ui-components';
import Image from 'next/image';
import mypic from './../../public/anhtule.jpeg';
import { LetsTalk } from '@journey-planner/shared-ui-components';
import { SocialMedias } from '@journey-planner/shared-ui-components';

export default function Home() {
  return (
    <div className="flex flex-1">
      <SocialMedias />
      <div className="flex flex-col md:flex-row py-10 flex-1">
        <div className="w-full h-full md:w-2/5  pl-5 flex flex-col justify-center">
          <TypingAnimation />
          <div className="font-bold mt-5 text-gray-700">
            Software Engineer | Project manager
          </div>

          <div className="mt-10">
            Anh Tu Le - Software developer, who has handed on many web and
            mobile projects with public sector clients
          </div>
          <div className="mt-20 flex justify-center items-center mb-2">
            <LetsTalk />
          </div>
        </div>
        <div className="flex-1 flex justify-center items-center ">
          <div className="w-[300px] h-[300px] md:w-[350px] md:h-[350px] lg:w-[400px] lg:h-[400px]">
            <Image src={mypic} alt="avatar" className="rounded-full" />
          </div>
        </div>
      </div>
    </div>
  );
}
