'use client';
import React from 'react';
import { PorfolioInfoItem, LanguageSkills } from '@journey-planner/porfolio';
import Image from 'next/image';
import mypic from './../../../public/anhtule.jpeg';
import mypic2 from './../../../public/anhtule2.png';
import { TechSkillItem, WhatIBuildAnimation } from '@journey-planner/porfolio';
import { ExperienceItem } from '@journey-planner/porfolio';
import { ProjectBox } from '@journey-planner/porfolio';

const techData = [
  {
    label: 'HTML/CSS/Tailwind',
    percentage: 100,
  },
  {
    label: 'Javascript/Typescript',
    percentage: 100,
  },
  {
    label: 'React/Redux/Next.js',
    percentage: 100,
  },
  {
    label: 'GraphQL',
    percentage: 85,
  },
  {
    label: 'Node.js',
    percentage: 90,
  },
  {
    label: 'SQL/PosgreSQL/MongoDB',
    percentage: 90,
  },
  {
    label: 'DevOps: Gitlab CI/CD',
    percentage: 85,
  },
  {
    label: 'Docker',
    percentage: 85,
  },
  {
    label: 'Jest/Cypress',
    percentage: 85,
  },
  {
    label: 'Amazon web services (AWS)',
    percentage: 65,
  },
];
const languages = [
  { percentage: 100, language: 'Vietnamese' },
  { percentage: 100, language: 'English' },
  { percentage: 20, language: 'Finnish' },
];

const companies = [
  {
    id: 1,
    name: 'HSL Helsingin seudun liikenne',
    role: 'Software developer',
    description: `* [x] Working in HSL app team: maintenance and develop new feature (HSL app)`,
  },
  {
    id: 2,
    name: 'Vertics',
    role: 'Full-stack Developer',
    description: `* [x] done Engineered modern applications with JavaScript, SQL Server, and No SQL.\n* [x] Worksite management system \n* [x] ERP ( ReactJS and Ruby) \n* [x] Form management system for owners and contractors (ReactJS, Node) \n* [x] Game ranking management mobile application (React Native, Node) \n* [x] Finnish mobility as a service provider like Uber (React Native, Node, Firebase) \n* [x] Form management system (ReactJS, Node) \n* [x] Ticket management system for art concerts (ReactJS + Node)`,
  },
  {
    id: 3,
    name: 'Nordhealth',
    role: 'Fullstack developer',
    description:
      '* [x] Taking care of responsibilities to develop a mobile application by React Native for management purpose. \n* [x] Efficiently deployed and integrated software engineered by team and updated integration/deployment scripts to improve continuous integration practices.',
  },
  {
    id: 4,
    name: 'Trail System',
    role: 'Software developer',
    description:
      'Main responsibilities is to take care of frontend software development but also include a wider set of other development',
  },
];

const Porfolio = () => {
  return (
    <div className="w-full my-5 p-5 bg-[#191923] rounded">
      <div className="w-full flex bg-[#1E1E28]">
        <div className="w-1/4 flex flex-col shadow-white/10 shadow-lg min-w-[300px]">
          {/* info */}
          <div className="info bg-[#242530] flex flex-col justify-center items-center py-8">
            <Image
              src={mypic}
              alt="avatar"
              className="rounded-full"
              width={150}
              height={150}
            />
            <span className="text-white text-lg mt-5">Anh Tu le</span>
            <span className="text-sm text-[#737477]">Software engineer</span>
          </div>
          {/* content */}
          <div className="flex-1">
            <div className="flex-1  mx-5 py-5 border-b-2 border-[#737477]">
              <PorfolioInfoItem label="Residence" value="Finland" />
              <PorfolioInfoItem label="City" value="Espoo" />
              <PorfolioInfoItem label="Age" value="30" />
            </div>
            <div className="flex justify-evenly mx-5 py-5 border-b-2 border-[#737477]">
              {languages.map(({ percentage, language }, key) => (
                <LanguageSkills
                  key={key}
                  percentage={percentage}
                  language={language}
                />
              ))}
            </div>
            <div className="mx-5 py-5 border-b-2 border-[#737477]">
              {techData.map(({ percentage, label }, key) => (
                <TechSkillItem
                  key={key}
                  percentage={percentage}
                  className="mb-5"
                  label={label}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="flex-1 ">
          <div className=" shadow-white/10 shadow-lg  m-5 flex flex-col bg-cover relative  bg-no-repeat h-[350px] bg-[url('https://cdn.pixabay.com/photo/2017/09/21/14/47/moutain-2772109_960_720.jpg')]">
            <div className="absolute w-full h-full bg-black/75 flex pl-10 ">
              <div className="absolute flex flex-col h-full items-start justify-center z-2">
                <WhatIBuildAnimation />
                <div className="bg-[#EFBB35] py-2 px-10 mt-5 cursor-pointer">
                  Explore now
                </div>
              </div>
              <Image
                src={mypic2}
                alt="avatar"
                width={350}
                height={450}
                className="absolute bottom-0 h-[365px] right-4 z-1 "
              />
            </div>
          </div>
          <div className="flex items-center justify-between mx-5">
            <ExperienceItem stat={5} label="Years experience" suffix="+" />
            <ExperienceItem stat={10} label="Completed projects" />
            <ExperienceItem stat={2} label="Awards" />
          </div>
          <div className="m-5  w-full grid grid-cols-2">
            {companies.map(({ id, name, role, description }) => (
              <ProjectBox
                key={id}
                company={name}
                role={role}
                description={description}
                className="mr-5 mb-5 "
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Porfolio;
