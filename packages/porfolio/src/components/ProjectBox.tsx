import React from 'react';
import Markdown from './Markdown';

type Props = {
  company: string;
  role: string;
  description: string;
  className?: string;
};
const ProjectBox = ({ company, role, description, className }: Props) => {
  return (
    <div className={`shadow-2xl bg-[#2C2C38] p-5 ${className}`}>
      <div className="text-white text-lg">{company}</div>
      <div className="text-[#737477] text-sm mb-3">{role}</div>
      <div className="text-sm text-[#737477]">
        <Markdown markdown={description} />
      </div>
    </div>
  );
};

export default ProjectBox;
