import CustomCircularProgress from '@journey-planner/shared-ui-components/src/components/CustomCircularProgress';
import React from 'react';

type Props = {
  percentage: number;
  language: string;
};
const LanguageSkills = ({ percentage, language }: Props) => {
  return (
    <div className="flex flex-col justify-center items-center mx-2 ">
      <CustomCircularProgress percentage={percentage} width={50} height={50} />
      <div className="text-white mt-3 text-xs">{language}</div>
    </div>
  );
};

export default LanguageSkills;
