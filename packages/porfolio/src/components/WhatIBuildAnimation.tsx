import React from 'react';
import Typewriter from 'typewriter-effect';

const WhatIBuildAnimation = () => {
  return (
    <div className="flex justify-center items-center">
      <span className="text-white text-xl">{`<`}</span>
      <span className="text-xl text-[#EFBB35]">code</span>
      <span className="text-white text-xl">{`>`}</span>
      <Typewriter
        options={{
          loop: true,
          skipAddStyles: false,
          wrapperClassName: 'text-white text-2xl font-bold',
          cursorClassName: 'text-white text-2xl',
        }}
        onInit={(typewriter) => {
          typewriter
            .typeString('I build mobile and web applications')
            .callFunction(() => {
              console.log('String typed out!');
            })
            .pauseFor(1000)
            .deleteChars(27)
            .callFunction(() => {
              console.log('All strings were deleted');
            })
            .typeString('ERP')
            .pauseFor(3000)
            .start();
        }}
      />
      <span className="text-white text-xl ">{`</`}</span>
      <span className="text-xl text-[#EFBB35]">code</span>
      <span className="text-white text-xl ">{`>`}</span>
    </div>
  );
};

export default WhatIBuildAnimation;
