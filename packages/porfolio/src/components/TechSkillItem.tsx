'use client';
import React from 'react';
import ProgressBar from '@ramonak/react-progress-bar';
import PorfolioInfoItem from './PorfolioInfoItem';
import { useAutoIncrement } from '@journey-planner/utils';
type Props = {
  label: string;
  className: string;
  percentage: number;
};
const TechSkillItem = ({ label, className, percentage }: Props) => {
  const number = useAutoIncrement(percentage);

  return (
    <div className={className}>
      <PorfolioInfoItem label={label} value={`${percentage}%`} />
      <div className="mt-2">
        <ProgressBar
          completed={number}
          className="text-gray"
          bgColor="#EFBB35"
          height="15px"
          labelSize="10px"
          labelColor="#737477"
          isLabelVisible={false}
        />
      </div>
    </div>
  );
};

export default TechSkillItem;
