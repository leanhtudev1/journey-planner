import React from 'react';
type Props = {
  label: string;
  value: string;
};
const PorfolioInfoItem = ({ label, value }: Props) => {
  return (
    <div className="flex justify-between text-sm">
      <div className="text-white">{label}</div>
      <div className="text-[#737477]">{value}</div>
    </div>
  );
};

export default PorfolioInfoItem;
