'use client';
import { useAutoIncrement } from '@journey-planner/utils';
import React from 'react';
type Props = {
  stat: number;
  label: string;
  suffix?: string;
};
const ExperienceItem = ({ stat, label, suffix }: Props) => {
  const number = useAutoIncrement(stat, 50);
  return (
    <div className="flex items-center ">
      <span className="text-xl text-[#EFBB35] mr-3">
        {number} {suffix}
      </span>
      <span className="text-white">{label}</span>
    </div>
  );
};

export default ExperienceItem;
