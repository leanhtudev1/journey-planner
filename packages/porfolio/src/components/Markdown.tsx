import React from 'react';
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';

type Props = {
  markdown: string;
};
const Markdown = ({ markdown }: Props) => {
  return (
    <div>
      {/* eslint-disable-next-line */}
      <ReactMarkdown children={markdown} remarkPlugins={[remarkGfm]} />
    </div>
  );
};

export default Markdown;
