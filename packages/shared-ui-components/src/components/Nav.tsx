'use client';
import React, { useCallback } from 'react';
import Link from 'next/link';
import HoverAnimation from './HoverAnimation';
import Image from 'next/image';
import { usePathname } from 'next/navigation';

const Nav = () => {
  const pathname = usePathname();
  const renderMenus = useCallback(() => {
    const menus = [
      { id: 1, label: 'Home', value: '/', icon: './home.svg' },
      { id: 2, label: 'Porfolio', value: '/porfolio', icon: './resume.svg' },
      {
        id: 3,
        label: 'Journey Planner',
        value: '/journey-planner',
        icon: './journey.svg',
      },
    ];

    return menus.map(({ id, label, value, icon }) => {
      return (
        <HoverAnimation
          key={id}
          className={`mx-2 flex ${
            pathname === value ? 'bg-red-400 text-white' : ''
          }`}
        >
          <Image
            src={icon}
            alt={label}
            width={24}
            height={24}
            className="mr-1"
          />
          <Link href={value}>{label}</Link>
        </HoverAnimation>
      );
    });
  }, [pathname]);

  return (
    <div className="flex-row justify-between pt-5 items-center sm:block lg:flex px-[10px]">
      <div className="font-mono text-4xl font-bold">
        anh<span className="text-red-500">tu</span>le
      </div>
      <div className="justify-center md:justify-end flex-1 flex flex-row ">
        {renderMenus()}
      </div>
    </div>
  );
};

export default Nav;
