'use client';
import { useCallback } from 'react';
import Image from 'next/image';

const SocialMedias = () => {
  const onNavigateNewTab = useCallback((url: string) => {
    window.open(url, '_blank');
  }, []);

  const renderData = useCallback(() => {
    const data = [
      {
        id: 1,
        url: 'https://www.linkedin.com/in/anh-tu-le-180b00153/',
        icon: './linkedin.svg',
        label: 'Linkedin',
      },
      {
        id: 2,
        url: 'https://gitlab.com/leanhtudev1',
        icon: './gitlab.svg',
        label: 'Gitlab',
      },
      {
        id: 3,
        url: 'https://github.com/leanhtudev',
        icon: './github.svg',
        label: 'Github',
      },
    ];
    return data.map(({ url, icon, label, id }) => (
      <div
        key={id}
        onClick={() => onNavigateNewTab(url)}
        className="cursor-pointer"
      >
        <Image src={icon} alt={label} width={40} height={40} className="mb-5" />
      </div>
    ));
  }, [onNavigateNewTab]);

  return (
    <div className="flex flex-col justify-center items-center px-5">
      {renderData()}
    </div>
  );
};

export default SocialMedias;
