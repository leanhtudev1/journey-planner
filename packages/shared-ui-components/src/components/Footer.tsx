'use client';
import React, { useCallback } from 'react';
import HoverAnimation from './HoverAnimation';
import { onWindowActions } from '@journey-planner/utils';
const Footer = () => {
  return (
    <div className="flex justify-end pb-5 items-center sm:block lg:flex px-[10px]">
      <HoverAnimation
        onClick={(e) => onWindowActions(e, 'mailto', 'leanhtu.ufm@gmail.com')}
        className="text-gray-500 pr-2 mr-2"
      >
        leanhtu.ufm@gmail.com
      </HoverAnimation>
      <div className="text-gray-500"> |</div>
      <HoverAnimation
        onClick={(e) => onWindowActions(e, 'tel', '+358503101245')}
        className="text-gray-500 pr-2 mx-2"
      >
        +358503101245
      </HoverAnimation>
      <div className="text-gray-500 mr-2"> |</div>

      <div className="text-gray-500">
        Anna Sahlsténin Katu, 02600, Espoo, Finland
      </div>
    </div>
  );
};

export default Footer;
