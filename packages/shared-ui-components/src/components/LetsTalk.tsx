'use client';

import React from 'react';

const LetsTalk = () => {
  return (
    <div className="w-[150px] h-[150px] bg-red-200 cursor-pointer rounded-full flex  justify-center items-center">
      <div className="w-[90px] h-[90px] bg-red-500 rounded-full flex font-light justify-center items-center text-white">
        Let's talk
      </div>
    </div>
  );
};

export default LetsTalk;
