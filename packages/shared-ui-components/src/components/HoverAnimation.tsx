import React, { ReactNode } from 'react';
type Props = {
  children: ReactNode;
  className?: string;
  onClick?: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
};
const HoverAnimation = ({ children, className, onClick }: Props) => {
  return (
    <div
      className={`py-1 px-2 rounded cursor-pointer hover:bg-red-400 hover:-rotate-7  transition-transform hover:duration-700 hover:text-white ${className}`}
      onClick={onClick}
    >
      {children}
    </div>
  );
};

export default HoverAnimation;
