'use client';

import { useAutoIncrement } from '@journey-planner/utils';
import React from 'react';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
type Props = { percentage: number; width?: number; height?: number };

const CustomCircularProgress = ({
  percentage,
  width = 50,
  height = 50,
}: Props) => {
  const number = useAutoIncrement(percentage);

  return (
    <div className={`w-[${width}px] h-[${height}px]`}>
      <CircularProgressbar
        value={number}
        text={`${number}%`}
        styles={{
          // Customize the path, i.e. the "completed progress"
          path: {
            // Path color
            stroke: `#EFBB35`,
          },

          // Customize the text
          text: {
            // Text color
            fill: '#737477',
            // Text size
            fontSize: '20px',
          },
          // Customize background - only used when the `background` prop is true
        }}
        strokeWidth={10}
      />
    </div>
  );
};

export default CustomCircularProgress;
