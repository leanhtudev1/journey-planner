'use client';
import Typewriter from 'typewriter-effect';

const TypingAnimation = () => {
  return (
    <Typewriter
      options={{
        loop: true,
        skipAddStyles: false,
        wrapperClassName: 'text-gray-500 text-5xl font-bold',
        cursorClassName: 'text-gray-500 text-5xl',
      }}
      onInit={(typewriter) => {
        typewriter
          .typeString('Hi, I am Anh Tu Le')
          .callFunction(() => {
            console.log('String typed out!');
          })
          .pauseFor(1000)
          .deleteAll()
          .callFunction(() => {
            console.log('All strings were deleted');
          })
          .typeString("Let's connect!")
          .pauseFor(3000)
          .start();
      }}
    />
  );
};

export default TypingAnimation;
