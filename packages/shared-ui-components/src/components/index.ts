export { default as Nav } from './Nav';
export { default as HoverAnimation } from './HoverAnimation';
export { default as TypingAnimation } from './TypingAnimation';
export { default as SocialMedias } from './SocialMedias';
export { default as LetsTalk } from './LetsTalk';
export { default as Footer } from './Footer';
