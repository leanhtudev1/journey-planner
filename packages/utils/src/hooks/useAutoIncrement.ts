import { useEffect, useState } from 'react';

export const useAutoIncrement = (input: number, delay?: number) => {
  const [output, setOutput] = useState(0);

  useEffect(() => {
    console.log(output, 'oke');

    if (output < input ) {
      setTimeout(() => {
        setOutput(output + 1);
      }, delay);
    }
  }, [output, input, delay]);

  return output;
};
