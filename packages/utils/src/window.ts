import React from 'react';

export const onWindowActions = (
  e: React.MouseEvent<HTMLDivElement, MouseEvent>,
  type: 'mailto' | 'tel',
  value: string
) => {
  try {
    window.location.href = `${type}:${value}`;
    e.preventDefault();
  } catch (e) {
    alert('Value not correct for window function');
  }
};
