/** @type {import('next').NextConfig} */
const nextConfig = {
  compilerOptions: {
    baseUrl: '.',
    paths: {
      '@/public/*': ['public/*'],
    },
  },
};

module.exports = nextConfig;
